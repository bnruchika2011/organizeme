import Vue from 'vue';
import firebase from 'firebase';
import App from './App.vue';
import router from './router';

Vue.config.productionTip = false;

const config = {
  apiKey: 'AIzaSyD1sX0wTejmpkyY-2L9Y39Hz9wkVQANDZo',
  authDomain: 'organizeme-187ac.firebaseapp.com',
  databaseURL: 'https://organizeme-187ac.firebaseio.com',
  projectId: 'organizeme-187ac',
  storageBucket: 'organizeme-187ac.appspot.com',
  messagingSenderId: '586370329353',
  appId: '1:586370329353:web:8db2fa5d5792f08c',
};

firebase.initializeApp(config);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
